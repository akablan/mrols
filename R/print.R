#' @describeIn paraOLS Affiche un objet de classe \code{"paraOLS"}
#'
#' @param x Un objet output de la fonction \code{paraOLS}.
#' @param ... additionnal arguments.

#' @export
#'
print.paraOLS <- function(x, ...){
  cat("Estimation results\n")
  cat("-------------------\n")
  print(round(x[,1],5))
}

